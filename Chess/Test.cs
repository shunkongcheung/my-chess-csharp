﻿using System;
using System.IO;
using System.Threading;

namespace Chess
{
    class Test
    {
        // setting output and performance
        // initial value irrelevant
        private static bool PrintAnalyzer = false;
        private static bool RunMulti = true;
        private static bool RunRandom = false;

        // which section should be run
        private const bool IsFunctionality = false;
        private const bool IsOptimization = false;
        private const bool IsAnalysis = false;

        // output file name
        private static string logFileename;
        private static Mutex LockFile = new Mutex();

        public static void Testing()
        {
            logFileename = "log/" + DateTime.Now.ToString("hhmmssfff") + ".txt";
            bool tempPrint = PrintAnalyzer;
            bool tempMulti = RunMulti;
            bool tempRand = RunRandom;

            // the following are for testing functionality
            if (IsFunctionality)
            {
                // testing choose valid
                PrintAnalyzer = false;
                RunMulti = false;
                OutputString("ChooseValid: Testing sequential.. ");
                TestChooseValid();

                PrintAnalyzer = false;
                RunMulti = true;
                OutputString("ChooseValid: Testing multithread.. ");
                TestChooseValid();

                // test sequential and multithread
                PrintAnalyzer = false;
                RunRandom = false;
                TestMultiMatchSequential(30, "init/startboard.txt");
            }
            if (IsOptimization)
            {
                // check which threadnum and maxcount best suit
                PrintAnalyzer = false;
                RunMulti = true;
                //TestMaxCount("init/startboard.txt", 80000, 500, 5000);
                
                TestMuliThreadParameter("init/startboard.txt", 80000);

            }
            if (IsAnalysis)
            {
                // create form
                GameContent.Analyzer = new AnalyzerForm();
                GameContent.Analyzer.Show();
                SimulateChoosing("init/startboard.txt", 100);// AnalyzerForm.TreeMaxSize);
            }

            // finish. reet to original setting
            PrintAnalyzer = tempPrint;
            RunMulti = tempMulti;
            RunRandom = tempRand;

            if (PrintAnalyzer)
            {
                GameContent.Analyzer = new AnalyzerForm();
                GameContent.Analyzer.Show();
            }
        }

        public static bool IsRunAnalyzer() { return PrintAnalyzer; }
        public static bool IsRunMulti() { return RunMulti; }
        public static bool IsRandoom() { return RunRandom; }

        public static void OutputBoard(Board board)
        {
            StringWriter strWriter = new StringWriter();
            strWriter.WriteLine("------------------------");

            for (int i = 0; i < Board.BoardHeight; i++)
            {
                for (int j = 0; j < Board.BoardWidth; j++)
                {
                    if (board.GetChessPlayer(i, j) == Board.Player.Top) strWriter.Write("{0}", (char)board.GetChessName(i, j));
                    if (board.GetChessPlayer(i, j) == Board.Player.Bot) strWriter.Write("{0}", (char)(board.GetChessName(i, j) - 'A' + 'a'));
                    if (board.GetChessPlayer(i, j) == Board.Player.Empt) strWriter.Write(".");
                }
                strWriter.WriteLine() ;
            }

            OutputString(strWriter.ToString());
            OutputString("------------------------");
        }

        public static void OutputString(String message)
        {
            LockFile.WaitOne();
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(logFileename, true))
            {
                file.WriteLine(DateTime.Now.ToString("hh:mm:ss") + ": " + message);
            }
            Console.WriteLine(message);
            LockFile.ReleaseMutex();
        }

        public static void OutputChoice(Choice choice, Board board)
        {
            // move
            StringWriter strwriter = new StringWriter();
            strwriter.Write("Choice: ({0}, {1}: {2})-> ({3},{4}: {5})",
                choice.fromrow, choice.fromcol, board.GetChessName(choice.fromrow, choice.fromcol),
                choice.torow, choice.tocol, board.GetChessName(choice.torow, choice.tocol)
                );
            OutputString(strwriter.ToString());

            // expectation
            strwriter = new StringWriter();
            strwriter.Write("Expectation: [{0}]. ({1}, {2}).",
                choice.winner,
                choice.boardValue.GetTopVal(), choice.boardValue.GetBotVal()
                );

            OutputString(strwriter.ToString());

            // board
            OutputBoard(board);
        }


        private static void TestChooseValid()
        {
            Board expectedboard = new Board("init/z_chasing.txt");
            Choice chaset = Choose.MakeChoice(expectedboard, Board.Player.Top);
            Console.WriteLine("ChooseValid: finish chase top expectation test..\n");

            Choice chaseb = Choose.MakeChoice(expectedboard, Board.Player.Bot);
            Console.WriteLine("ChooseValid: finish chase bottom expectation test..\n");


            expectedboard = new Board("init/z_dead.txt");
            Choice deadt = Choose.MakeChoice(expectedboard, Board.Player.Top);

            Console.WriteLine("ChooseValid: finish dead top expectation test..\n");

            Choice deadb = Choose.MakeChoice(expectedboard, Board.Player.Bot);
            Console.WriteLine("ChooseValid: finish dead bottom expectation test..\n");


            expectedboard = new Board("init/z_middle.txt");
            Choice middlet = Choose.MakeChoice(expectedboard, Board.Player.Top);

            Console.WriteLine("ChooseValid: finish middle top expectation test..\n");

            Choice middleb = Choose.MakeChoice(expectedboard, Board.Player.Bot);
            Console.WriteLine("ChooseValid: finish middle bottom expectation test..\n");

            expectedboard = new Board("init/z_uptoturn.txt");
            Choice turnt = Choose.MakeChoice(expectedboard, Board.Player.Top);
            Console.WriteLine("ChooseValid: finish turn top expectation test..\n");


            Choice turnb = Choose.MakeChoice(expectedboard, Board.Player.Bot);
            Console.WriteLine("ChooseValid: finish turn bottom expectation test..\n");


            if (chaset.winner == Board.Player.Top && chaseb.winner == Board.Player.Empt &&
                deadt.winner == Board.Player.Top && deadb.winner == Board.Player.Top &&
                middlet.winner == Board.Player.Empt && middleb.winner == Board.Player.Empt &&
                turnt.winner == Board.Player.Top && turnb.winner == Board.Player.Bot)

                OutputString("ChooseValid: Success. Result output as expected.\n");
            else
            {
                OutputString("ChooseValid: ERROR! Result Unexpected. ");

                StringWriter strwriter = new StringWriter();
                strwriter.Write("chaset(Top, {0}) chaseb(Empt, {1}) deadt(Top, {2}) deadb(Top, {3}) ",
                   chaset.winner, chaseb.winner,
                   deadt.winner, deadb.winner);

                OutputString(strwriter.ToString());

                strwriter = new StringWriter();
                strwriter.Write("middlet(Empt, {0}) middleb(Empt, {1}) turnt(Top, {2}) turnb(Bot,{3}).\n",
                   middlet.winner, middleb.winner,
                   turnt.winner, turnb.winner);

                OutputString(strwriter.ToString());
            }
        }

        private static bool IsChoiceEqual(Choice c1, Choice c2)
        {
            return (c1.fromrow == c2.fromrow && c1.fromcol == c2.fromcol
                     && c1.torow == c2.torow && c1.tocol == c2.tocol
                     && c1.winner == c2.winner

                     && c1.boardValue.GetTopVal() == c2.boardValue.GetTopVal()
                     && c1.boardValue.GetBotVal() == c2.boardValue.GetBotVal()
                     && c1.boardValue.GetWeighted() == c2.boardValue.GetWeighted()
                );
        }
        private static ChoiceList GetChoiceSequence(int stepmax, string filename)
        {
            // start parameter
            Board.Player side = Board.Player.Top;
            Board board = new Board(filename);

            // result
            ChoiceList head = null;

            // run for some step
            for (int step = 0; step < stepmax; step++)
            {
                Console.WriteLine("MulltiVsSequen: running step {0}..", step);
                ChoiceList choiceList = new ChoiceList(Choose.MakeChoice(board, side), new Board(board))
                {
                    next = head
                };
                head = choiceList;

                // randomly to move master board
                if (head.choice.fromrow != -1)
                {
                    board.MoveBoard(head.choice.fromrow, head.choice.fromcol, head.choice.torow, head.choice.tocol);
                    side = (side == Board.Player.Top) ? Board.Player.Bot : Board.Player.Top;
                }
                else
                    break;
            }
            return head;
        }
        private static void TestMultiMatchSequential(int testTime, string filename)
        {
            // get sequential result
            PrintAnalyzer = false;
            RunMulti = false;
            ChoiceList sequential = GetChoiceSequence(testTime, filename);

            // get multithread result
            RunMulti = true;
            ChoiceList multithread = GetChoiceSequence(testTime, filename);

            // check if all matches
            int step = 0;
            while (sequential != null && multithread != null)
            {
                // check if they are same
                if (IsChoiceEqual(sequential.choice, multithread.choice) == false)
                {

                    StringWriter strwriter = new StringWriter();
                    strwriter.Write("MulltiVsSequen: ERROR! Not matching at step {0}.", step);
                    Test.OutputString(strwriter.ToString());

                    Test.OutputString("Sequential:");
                    OutputChoice(sequential.choice, sequential.board);

                    Test.OutputString("MultiThread:");
                    OutputChoice(multithread.choice, multithread.board);
                    Test.OutputString("");

                    return;
                }

                // next
                sequential = sequential.next;
                multithread = multithread.next;
                step++;
            }

            // finish
            StringWriter str2 = new StringWriter();
            str2.Write("MulltiVsSequen: Success. Run {0} steps. All sequential and multithread output matches.", step);
            OutputString(str2.ToString());
        }

        private static int TestMaxCount(String filename,int suggestMaxStart, int suggestMaxChange,long requiedTime)
        {
            // look for best combination of threadnum and maxcount that will fulfill the time limit
            StringWriter str = new StringWriter();
            OutputString("OptimizeTest: Start Running...");

            int maxcount = suggestMaxStart;

            // want maxcount to be as large as possible. No depth / self limit
            for (; maxcount > 0; maxcount -= suggestMaxChange)
            {
                Console.WriteLine("OptimizeTest: running [{0}max]...", maxcount);

                // make choice and record time
                var watch = System.Diagnostics.Stopwatch.StartNew();
                Choose.TestParmeter(maxcount, 100, 100);
                Choice choice = Choose.MakeChoice(new Board(filename), Board.Player.Top);
                long totaltime = watch.ElapsedMilliseconds;

                // compare the ratio and time
                if (totaltime <= requiedTime)
                {
                    str = new StringWriter();
                    str.Write("OptimizeTest: Fulfill Time requirement [{0}ms at [{1}max]\n", totaltime, maxcount);
                    OutputString(str.ToString());
                    return maxcount;
                }
            }
            return -1;
        }

        private static void TestMuliThreadParameter(String filename, int maxcount)
        {
            // look for best combination of threadnum and maxcount that will fulfill the time limit
            StringWriter str = new StringWriter();
            OutputString("OptimizeTest: Start Running...");
            
            // want the limit to be as big as possible
            for (int limit = 100; limit > 0; limit--)
            {
                // want depth to be as deep as possible
                for (int depth = 7; depth >= 4; depth--)
                {
                    Console.WriteLine("OptimizeTest: running [{0}max, {1}depth, {2}limit]...", maxcount, depth, limit);

                    // make choice and record time
                    Choose.TestParmeter(maxcount, depth, limit);
                    Choice choice = Choose.MakeChoice(new Board(filename), Board.Player.Top);

                    // check if all calculatted
                    if ( choice.isAllCalculated )
                    {
                        str = new StringWriter();
                        str.Write("OptimizeTest: Fulfill! [{0}max, {1}depth, {2}limit]\n", maxcount, depth, limit);
                        OutputString(str.ToString());
                        break;
                    }
                    
                }
            }
        }

        private static void SimulateChoosing(String filename, int testTime)
        {
            // print xml. Need to run in sequential
            //PrintAnalyzer = true;
            //RunMulti = false;

            
            // starting parameter
            Board.Player side = Board.Player.Top;
            Board board = new Board(filename);

            int step = 0;
            for (; step < testTime; step++)
            {
                Console.WriteLine("GenerateXml: running step {0}..", step);

                // use seqential
                if (step % 2 == 0)
                    Choose.TestParmeter(80000, 5, 5);
                else
                    Choose.TestParmeter(80000, 4, 100);

                Choice choice = Choose.MakeChoice(board, side);

                GameContent.Analyzer.PushTreeList();

                String str = "Choose by [" + ((side == Board.Player.Top) ? "Top" : "Bot") + "Player, using "+ ((step % 2 == 0) ? "[5,5]" : "[4,infinite]") + " setting..";
                OutputString(str);
                OutputChoice(choice, board);


                // randomly to move master board
                if (choice.fromrow != -1)
                {
                    board.MoveBoard(choice.fromrow, choice.fromcol, choice.torow, choice.tocol);
                    side = (side == Board.Player.Top) ? Board.Player.Bot : Board.Player.Top;
                }
                else
                    break;
            }

            // finish
            StringWriter str2 = new StringWriter();
            str2.Write("GenerateXml: Finished. Run {0} steps.", step);
            OutputString(str2.ToString());
        }

    }

    class ChoiceList
    {
        public Choice choice;
        public Board board;
        public ChoiceList next;

        public ChoiceList(Choice iChoice, Board iBoard)
        {
            choice = iChoice;
            board = iBoard;
            next = null;
        }
    }
}
