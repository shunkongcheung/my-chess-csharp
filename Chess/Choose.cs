﻿using System;
using System.Threading;

namespace Chess
{
    class Choose
    {

        // result through optimize testing
        private const int defaultMaxCount =   80000;
        private const int difficultMaxCount = 100000;
        private const int defaultLimit = 10;
        private const int defaultDepth = 5;

        // this should be 80000, need to test for limit and depth
        private const int difficultLimit = 100;
        private const int diffDepth = 100;

        // reduce the depth to make it stupider
        private const int easyDepth = 4;

        // section
        private const int section = 2;

        // should be the number provided by constant above
        private const int numThread = 64;
        private static int maxcount = defaultMaxCount;
        private static int currDepth = defaultDepth;
        private static int limitSelfLevel = defaultLimit;


        public static void SetDifficulty(int diff)
        {
            switch (diff)
            {
                case 1: currDepth = easyDepth; maxcount = defaultMaxCount; limitSelfLevel = defaultLimit; break;
                case 2: currDepth = defaultDepth; maxcount = defaultMaxCount; limitSelfLevel = defaultLimit; break;
                case 3: currDepth = diffDepth; maxcount = difficultMaxCount; limitSelfLevel = difficultLimit; break;
            }
        }

        public static void TestParmeter(int iMaxCount, int iDepth, int iLimit)
        {
            // for testing only
            maxcount = iMaxCount;
            currDepth = iDepth;
            limitSelfLevel = iLimit;
        }


        public static Choice MakeChoice(Board board, Board.Player side)
        {
            // calculate expected size
            Move movelist = new Move();
            movelist.GetMoves(board, side);

            // calculate each thread handle how many move. Take the upper bound
            int interval = movelist.total / numThread;
            if (interval * numThread < movelist.total)
                interval += 1;

            // output testing
            if (Test.IsRunAnalyzer())
                AnalyzerTreeBuilder.PreSetting(currDepth, limitSelfLevel, side);

            // dynamic create choices
            Choice[] results = new Choice[numThread];
            for (int i = 0; i < numThread; i++)
                results[i] = new Choice(board, side);

            // start choosing
            StartChoosing(board, side, currDepth, interval, results, movelist);

            // choosing the best among results
            Choice result = new Choice(board, side);
            Choice.SetToWorseValue(result, side);
            result.isAllCalculated = true;

            // calculate best result
            result = results[GetBestChoice(results, board, numThread, side)];

            // get number of best result(s) and calculate data analysis
            int bestCount = 0;
            for (int i = 0; i < numThread; i++)
            {
                if (Choice.IsBetterChoice(results[i], result, side) == 0) bestCount++;
                result.isAllCalculated = result.isAllCalculated && results[i].isAllCalculated;
            }
            // random choice
            result = ChooseRandomly(result, results, bestCount, side);

            // output testing
            if (Test.IsRunAnalyzer())
            {
                BoardVal expectedWeight = new BoardVal(board, side);
                AnalyzerTreeBuilder.InsertNode(currDepth, result.fromrow, result.fromcol, result.torow, result.tocol, result.winner, result.boardValue, expectedWeight.GetWeighted(), board);
            }

            // return value
            return result;
        }

        private static int GetBestChoice(Choice[] choices, Board board, int length, Board.Player side)
        {
            // choosing the best among results
            Choice result = new Choice(board, side);
            Choice.SetToWorseValue(result, side);
            result.isAllCalculated = true;

            // no best result yet
            int bestIndex = 0;

            // loop through all results
            for (int i = 0; i < length; i++)
            {
                // compare result with current best result
                int compare = Choice.IsBetterChoice(choices[i], result, side);
                if (compare == 1) { Choice.CopyChoice(result, choices[i]); bestIndex = i; }
            }

            return bestIndex;
        }

        private static void StartChoosing(Board board, Board.Player side, int currDepth, int interval, Choice[] results, Move movelist)
        {
            // if sequential, calculate it one by one
            if (!Test.IsRunMulti())
            {
                for (int i = 0; i < numThread; i++)
                    ChooseForOneInterval(board, results, i, currDepth, side, interval, movelist);
                return;
            }

            // if multithread, calculate it together
            Thread[] threads = new Thread[numThread];
            for (int i = 0; i < numThread; i++)
            {
                threads[i] = new Thread(() => ChooseForOneInterval(board, results, i, currDepth, side, interval, movelist))
                {
                    Name = i.ToString()
                };
                threads[i].Start();
            }
            // wait for all thread to finish
            for (int i = 0; i < numThread; i++)
                threads[i].Join();

        }

        private static Choice ChooseRandomly(Choice bestchoice, Choice[] results, int bestCount, Board.Player side)
        {
            // if not random, do nothing
            if (Test.IsRandoom())
                return bestchoice;

            // Randomness
            Random rnd = new Random();

            // choose one random best choice
            int randBest = rnd.Next(bestCount) + 1;
            for (int i = 0; i < numThread; i++)
            {
                // if it is the best choice
                if (Choice.IsBetterChoice(results[i], bestchoice, side) == 0) { randBest--; }

                // chose this one
                if (randBest == 0) { Choice.CopyChoice(bestchoice, results[i]); break; }
            }
            return bestchoice;
        }

        private static void ChooseForOneInterval(Board inboard, Choice[] results, int iResultIndex, int depth, Board.Player side, int interval, Move movelist)
        {
            // local variable
            int resultIndex = (!Test.IsRunMulti()) ? iResultIndex : int.Parse(Thread.CurrentThread.Name);
            Board board = new Board(inboard);
            
            // handle my batch
            int start = interval * resultIndex;
            int end = interval * (resultIndex + 1);

            // data analysis
            int loopExpect = maxcount / (end - start + 1);
            results[resultIndex].isAllCalculated = true;

            // calculate recursively
            int curCalculated = 0;
            Board.Player nextside = (side == Board.Player.Top) ? Board.Player.Bot : Board.Player.Top;

            // create the wose choice
            Choice bestchoice = results[resultIndex];
            Choice.SetToWorseValue(bestchoice, side);

            // for all remaining
            for (int i = start; i < end && i < movelist.total; i++)
            {
              //  for data analysis
               if (curCalculated > (loopExpect * (i + 1 - start)))
                    {
                        Test.OutputString("Warning: not enough quota!");
                        results[resultIndex].isAllCalculated = false;
                    }

                // record the move
                Board.Player tempside = board.GetChessPlayer(movelist.torow[i], movelist.tocol[i]);
                Board.Role temprole = board.GetChessName(movelist.torow[i], movelist.tocol[i]);

                // move and calculated
                board.MoveBoard(movelist.fromrow[i], movelist.fromcol[i], movelist.torow[i], movelist.tocol[i]);
                Choice tempchoice = RecurChoose(board, nextside, depth - 1, true, ref curCalculated);

                // revert the change it made
                board.MoveBoard(movelist.torow[i], movelist.tocol[i], movelist.fromrow[i], movelist.fromcol[i]);
                board.SetBoard(movelist.torow[i], movelist.tocol[i], temprole, tempside);

                // a better choice
                if (Choice.IsBetterChoice(tempchoice, bestchoice, side) >= 0)
                {
                    // record the value
                    Choice.CopyChoice(bestchoice, tempchoice);

                    // record the move
                    bestchoice.fromrow = movelist.fromrow[i];
                    bestchoice.fromcol = movelist.fromcol[i];
                    bestchoice.torow = movelist.torow[i];
                    bestchoice.tocol = movelist.tocol[i];
                }
            }
        }

        private static Choice RecurChoose(Board board, Board.Player side, int depth, bool isEnemyLevel, ref int calCount)
        {
            // calculate this node. It should take no time. doesnt matter to calculate it first
            Choice tentativechoice = new Choice(board, side)
            {
                winner = board.Getwinner()
            };

            // used one quota for entering here
            calCount++;

            if (
                // if there is no result at first glance, need to calculate next level
                tentativechoice.winner == Board.Player.Empt && depth > 0 &&

                // if it is top layer, always proceed. If not, if it excceed, then do nothing
                (isEnemyLevel || maxcount > calCount))
            {

                // set to the worse case and try to find the best solution. 
                Choice.SetToWorseValue(tentativechoice, side);

                // for all possible next move, calculate the immediate score
                Move movelist = new Move();
                movelist.GetMoves(board, side);
                BoardVal[] scores = new BoardVal[movelist.total];
                for (int loop = 0; loop < movelist.total; loop++)
                {
                    // next board
                    Board.Player tempside = board.GetChessPlayer(movelist.torow[loop], movelist.tocol[loop]);
                    Board.Role temprole = board.GetChessName(movelist.torow[loop], movelist.tocol[loop]);

                    board.MoveBoard(movelist.fromrow[loop], movelist.fromcol[loop], movelist.torow[loop], movelist.tocol[loop]);

                    // get values
                    scores[loop] = new BoardVal(board, side);

                    // revert the change it made
                    board.MoveBoard(movelist.torow[loop], movelist.tocol[loop], movelist.fromrow[loop], movelist.fromcol[loop]);
                    board.SetBoard(movelist.torow[loop], movelist.tocol[loop], temprole, tempside);
                }

                // sort the immediate score
                SortMoves(movelist, scores, 0, movelist.total);

                // actual recursive call
                int next = (side == Board.Player.Top) ? -1 : 1;
                int recurloop = (side == Board.Player.Top) ? movelist.total - 1 : 0;
                Board.Player nextside = (side == Board.Player.Top) ? Board.Player.Bot : Board.Player.Top;

                // undeterministic
                int mycount = 0;
                for (; recurloop >= 0 && recurloop < movelist.total; recurloop += next)
                {

                    // next board
                    Board.Player tempside = board.GetChessPlayer(movelist.torow[recurloop], movelist.tocol[recurloop]);
                    Board.Role temprole = board.GetChessName(movelist.torow[recurloop], movelist.tocol[recurloop]);
                    board.MoveBoard(movelist.fromrow[recurloop], movelist.fromcol[recurloop], movelist.torow[recurloop], movelist.tocol[recurloop]);

                    // next result
                    Choice nextchoice = RecurChoose(board, nextside, depth - 1, !isEnemyLevel, ref calCount);

                    // revert the change it made
                    board.MoveBoard(movelist.torow[recurloop], movelist.tocol[recurloop], movelist.fromrow[recurloop], movelist.fromcol[recurloop]);
                    board.SetBoard(movelist.torow[recurloop], movelist.tocol[recurloop], temprole, tempside);

                    // compare two result
                    if (Choice.IsBetterChoice(nextchoice, tentativechoice, side) >= 0)
                    {
                        // copy choice
                        Choice.CopyChoice(tentativechoice, nextchoice);

                        // the position is used for debug only
                        tentativechoice.fromrow = movelist.fromrow[recurloop];
                        tentativechoice.fromcol = movelist.fromcol[recurloop];
                        tentativechoice.torow = movelist.torow[recurloop];
                        tentativechoice.tocol = movelist.tocol[recurloop];
                    }

                    // reaches best result
                    if (tentativechoice.winner == side)
                        break;

                    // second cutting point. Will exist no matter what. If it is losing. Treat it as undeterministic
                    if (!isEnemyLevel)
                    {
                        mycount++;
                        if (mycount >= limitSelfLevel && tentativechoice.winner == Board.Player.Empt)
                        {
                            break;
                        }
                    }

                    // this is level1. There is only one best choice here. 
                    if (depth == 1)
                        break;
                }
            }

            // output testing
            if (Test.IsRunAnalyzer())
            {
                BoardVal incomeBoardVal = new BoardVal(board, side);
                AnalyzerTreeBuilder.InsertNode(depth, tentativechoice.fromrow, tentativechoice.fromcol, tentativechoice.torow, tentativechoice.tocol,
                                                tentativechoice.winner, tentativechoice.boardValue, incomeBoardVal.GetWeighted(), board);
            }

            return tentativechoice;
        }

        private static void Swap(Move moves, BoardVal[] values, int i, int j)
        {
            // copy i to temp
            BoardVal tempvalue = values[i];
            int tempfr = moves.fromrow[i]; int tempfc = moves.fromcol[i];
            int temptr = moves.torow[i]; int temptc = moves.tocol[i];

            // copy j to i
            values[i] = values[j];
            moves.fromrow[i] = moves.fromrow[j]; moves.fromcol[i] = moves.fromcol[j];
            moves.torow[i] = moves.torow[j]; moves.tocol[i] = moves.tocol[j];

            // copy temp to j
            values[j] = tempvalue;
            moves.fromrow[j] = tempfr; moves.fromcol[j] = tempfc;
            moves.torow[j] = temptr; moves.tocol[j] = temptc;
        }

        private static void SortMoves(Move moves, BoardVal[] values, int start, int end)
        {
            if (start >= end) return;

            int pValue = values[start].GetWeighted();
            int pIndex = start;

            int i;
            int eIndex = start + 1;
            for (i = start + 1; i < end; i++)
            {
                // need exchange
                if (values[i].GetWeighted() < pValue)
                {
                    Swap(moves, values, i, eIndex);
                    eIndex++;
                }
            }
            // exchange eIndex with pivot
            eIndex--;

            Swap(moves, values, pIndex, eIndex);

            // recursive calls
            SortMoves(moves, values, start, eIndex);
            SortMoves(moves, values, eIndex + 1, end);
        }

    }
}
