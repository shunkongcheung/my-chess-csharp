﻿using System;

namespace Chess
{
    class Move
    {
        // setting to make thing easier
        private const int movemax = 60;

        // value for calculting moves
        public int total = 0;
        public int[] fromrow = new int[movemax];
        public int[] fromcol = new int[movemax];
        public int[] torow = new int[movemax];
        public int[] tocol = new int[movemax];

        public void GetMoves(Board board, Board.Player side)
        {
            bool memoryCheck = false;
            int i, j;
            for (i = 0; i < Board.BoardHeight; i++)
                for (j = 0; j < Board.BoardWidth; j++)
                {
                    // available end
                    if (board.GetChessPlayer(i, j) == side)
                    {
                        switch (board.GetChessName(i, j))
                        {
                            case (Board.Role.GENERAL): memoryCheck = GeneralMove(board, i, j) && memoryCheck; break;
                            case (Board.Role.CHARIOT): memoryCheck = ChariotMove(board, i, j) && memoryCheck; break;
                            case (Board.Role.HORSE): memoryCheck = HorseMove(board, i, j) && memoryCheck; break;
                            case (Board.Role.ARTILLERY): memoryCheck = ArtilleryMove(board, i, j) && memoryCheck; break;
                            case (Board.Role.ESCORT): memoryCheck = EscortMove(board, i, j) && memoryCheck; break;
                            case (Board.Role.JUMBO): memoryCheck = JumboMove(board, i, j) && memoryCheck; break;
                            case (Board.Role.SOLDIER): memoryCheck = SoldierMove(board, i, j) && memoryCheck; break;

                            default: break;
                        }
                    }
                }

            if (memoryCheck) { Test.OutputString("ERROR: Get Move memory is not large enough!\n"); }
        }

        public static bool IsMoveValid(Board board, Board.Player side, int fr, int fc, int tr, int tc)
        {
            Move possible = new Move();
            possible.GetMoves(board, side);

            bool isValid = false;
            for (int i = 0; i < possible.total && !isValid; i++)
                isValid = (possible.fromrow[i] == fr && possible.fromcol[i] == fc &&
                            possible.torow[i] == tr && possible.tocol[i] == tc);

            return isValid;
        }


        /* Private helper functions -----------------*/

        private bool Inbound(int row, int col, int left, int rite, int top, int bot)
        {
            // helper function for checking within boundary (may be board, or smaller square)
            return (top <= row && row < bot && left <= col && col < rite);
        }

        private bool InsertMove(int frow, int fcol, int trow, int tcol)
        {
            // inserting move into arrays
            if (total < movemax)
            {
                fromrow[total] = frow; fromcol[total] = fcol;
                torow[total] = trow; tocol[total] = tcol;
                total++;
                return true;
            }
            return false;
        }

        private bool GeneralMove(Board board, int row, int col)
        {

            const int left = 3; const int rite = 6;
            Board.Player myside = board.GetChessPlayer(row, col);
            int testrow, testcol;

            // left
            testrow = row; testcol = col - 1;
            if (Inbound(testrow, testcol, left, rite, 0, Board.BoardHeight) &&
                myside != board.GetChessPlayer(testrow, testcol))
            {
                if (InsertMove(row, col, testrow, testcol) == false) return false;
            }
            // rite
            testrow = row; testcol = col + 1;
            if (Inbound(testrow, testcol, left, rite, 0, Board.BoardHeight) &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false) return false;

            if (myside == Board.Player.Top)
            {
                // up
                const int upper = 3;
                testrow = row - 1; testcol = col;
                if (Inbound(testrow, testcol, left, rite, 0, upper) &&
                    myside != board.GetChessPlayer(testrow, testcol) &&
                    InsertMove(row, col, testrow, testcol) == false)
                    return false;
                // down
                testrow = row + 1; testcol = col;
                if (Inbound(testrow, testcol, left, rite, 0, upper) &&
                    myside != board.GetChessPlayer(testrow, testcol) &&
                    InsertMove(row, col, testrow, testcol) == false)
                    return false;

                // looking for fly general
                testrow = row + 1; testcol = col;
                for (; testrow < Board.BoardHeight; testrow++)
                {
                    if (board.GetChessName(testrow, testcol) == Board.Role.GENERAL &&
                        InsertMove(row, col, testrow, testcol) == false)
                        return false;
                    else if (board.GetChessPlayer(testrow, testcol) != Board.Player.Empt)
                        break;
                }
            }
            if (myside == Board.Player.Bot)
            {
                // up
                const int lower = 7;
                testrow = row - 1; testcol = col;
                if (Inbound(testrow, testcol, left, rite, lower, Board.BoardHeight) &&
                    myside != board.GetChessPlayer(testrow, testcol) &&
                    InsertMove(row, col, testrow, testcol) == false)
                    return false;
                // down
                testrow = row + 1; testcol = col;
                if (Inbound(testrow, testcol, left, rite, lower, Board.BoardHeight) &&
                    myside != board.GetChessPlayer(testrow, testcol) &&
                    InsertMove(row, col, testrow, testcol) == false)
                    return false;

                // looking for fly general
                testrow = row - 1; testcol = col;
                for (; testrow >= 0; testrow--)
                {
                    if (board.GetChessName(testrow, testcol) == Board.Role.GENERAL &&
                      InsertMove(row, col, testrow, testcol) == false)
                        return false;
                    else if (board.GetChessPlayer(testrow, testcol) != Board.Player.Empt)
                        break;
                }
            }
            return true;
        }

        private bool ChariotMove(Board board, int row, int col)
        {

            int i;
            Board.Player myside = board.GetChessPlayer(row, col);

            // search to left
            bool finish = false;
            for (i = (col - 1); i >= 0 && !finish; i--)
            {
                if (board.GetChessPlayer(row, i) != myside &&
                         InsertMove(row, col, row, i) == false)
                    return false;
                else if (board.GetChessPlayer(row, i) != Board.Player.Empt)
                    finish = true;
            }
            // search to right
            finish = false;
            for (i = (col + 1); i < Board.BoardWidth && !finish; i++)
            {
                if (board.GetChessPlayer(row, i) != myside &&
                       InsertMove(row, col, row, i) == false)
                    return false;
                else if (board.GetChessPlayer(row, i) != Board.Player.Empt)
                    finish = true;
            }
            // search to top
            finish = false;
            for (i = (row - 1); i >= 0 && !finish; i--)
            {
                if (board.GetChessPlayer(i, col) != myside &&
                    InsertMove(row, col, i, col) == false)
                    return false;
                else if (board.GetChessPlayer(i, col) != Board.Player.Empt)
                    finish = true;
            }
            // search to bottom
            finish = false;
            for (i = (row + 1); i < Board.BoardHeight && !finish; i++)
            {
                if (board.GetChessPlayer(i, col) != myside &&
                    InsertMove(row, col, i, col) == false)
                    return false;
                else if (board.GetChessPlayer(i, col) != Board.Player.Empt)
                    finish = true;
            }
            return true;
        }

        private bool HorseMove(Board board, int row, int col)
        {

            Board.Player myside = board.GetChessPlayer(row, col);
            int testrow, testcol, barrierrow, barriercol;
            // Up 1 left 2
            testrow = row - 1; testcol = col - 2;
            barrierrow = row; barriercol = col - 1;
            if (Inbound(testrow, testcol, 0, Board.BoardWidth, 0, Board.BoardHeight) &&
                board.GetChessPlayer(barrierrow, barriercol) == Board.Player.Empt &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;
            // up 1 right 2
            testrow = row - 1; testcol = col + 2;
            barrierrow = row; barriercol = col + 1;
            if (Inbound(testrow, testcol, 0, Board.BoardWidth, 0, Board.BoardHeight) &&
                board.GetChessPlayer(barrierrow, barriercol) == Board.Player.Empt &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;
            // Bot 1 left 2
            testrow = row + 1; testcol = col - 2;
            barrierrow = row; barriercol = col - 1;
            if (Inbound(testrow, testcol, 0, Board.BoardWidth, 0, Board.BoardHeight) &&
                board.GetChessPlayer(barrierrow, barriercol) == Board.Player.Empt &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;
            // bot 1 right 2
            testrow = row + 1; testcol = col + 2;
            barrierrow = row; barriercol = col + 1;
            if (Inbound(testrow, testcol, 0, Board.BoardWidth, 0, Board.BoardHeight) &&
                board.GetChessPlayer(barrierrow, barriercol) == Board.Player.Empt &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;
            // Up 2 left 2
            testrow = row - 2; testcol = col - 1;
            barrierrow = row - 1; barriercol = col;
            if (Inbound(testrow, testcol, 0, Board.BoardWidth, 0, Board.BoardHeight) &&
                board.GetChessPlayer(barrierrow, barriercol) == Board.Player.Empt &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;
            // up 2 right 1
            testrow = row - 2; testcol = col + 1;
            barrierrow = row - 1; barriercol = col;
            if (Inbound(testrow, testcol, 0, Board.BoardWidth, 0, Board.BoardHeight) &&
                board.GetChessPlayer(barrierrow, barriercol) == Board.Player.Empt &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;
            // Bot 2 left 1
            testrow = row + 2; testcol = col - 1;
            barrierrow = row + 1; barriercol = col;
            if (Inbound(testrow, testcol, 0, Board.BoardWidth, 0, Board.BoardHeight) &&
                board.GetChessPlayer(barrierrow, barriercol) == Board.Player.Empt &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;
            // bot 2 right 1
            testrow = row + 2; testcol = col + 1;
            barrierrow = row + 1; barriercol = col;
            if (Inbound(testrow, testcol, 0, Board.BoardWidth, 0, Board.BoardHeight) &&
                board.GetChessPlayer(barrierrow, barriercol) == Board.Player.Empt &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;

            return true;
        }

        private bool ArtilleryMove(Board board, int row, int col)
        {

            int i;
            Board.Player myside = board.GetChessPlayer(row, col);

            // search to left
            bool finish = false;
            bool barrier = false;
            for (i = (col - 1); i >= 0 && !finish; i--)
            {

                if (!barrier && board.GetChessPlayer(row, i) == Board.Player.Empt &&
                    InsertMove(row, col, row, i) == false)
                    return false;

                if (barrier)
                {
                    // barrier and see my side. finish
                    if (board.GetChessPlayer(row, i) == myside)
                        finish = true;

                    // barrier and see enemy. finish
                    else if (board.GetChessPlayer(row, i) != Board.Player.Empt)
                        if (InsertMove(row, col, row, i) == false)
                            return false;
                        else
                            finish = true;
                }

                // see a barrier
                if (board.GetChessPlayer(row, i) != Board.Player.Empt)
                    barrier = true;

            }

            // search to right
            finish = false;
            barrier = false;
            for (i = (col + 1); i < Board.BoardWidth && !finish; i++)
            {

                if (!barrier && board.GetChessPlayer(row, i) == Board.Player.Empt &&
                    InsertMove(row, col, row, i) == false)
                    return false;

              
                if (barrier)
                {
                    // barrier and see my side. finish
                    if (board.GetChessPlayer(row, i) == myside)
                        finish = true;

                    // barrier and see enemy. finish
                    else if ( board.GetChessPlayer(row, i) != Board.Player.Empt)
                        if (InsertMove(row, col, row, i) == false)
                            return false;
                        else
                            finish = true;
                }
                    

                // see a barrier
                if (board.GetChessPlayer(row, i) != Board.Player.Empt)
                    barrier = true;
            }

            // search to top
            finish = false;
            barrier = false;
            for (i = (row - 1); i >= 0 && !finish; i--)
            {
                if (!barrier && board.GetChessPlayer(i, col) == Board.Player.Empt &&
                    InsertMove(row, col, i, col) == false)
                    return false;
                if (barrier)
                {
                    // barrier and see my side. finish
                    if (board.GetChessPlayer(i, col) == myside)
                        finish = true;

                    // barrier and see enemy. finish
                    else if (board.GetChessPlayer(i, col) != Board.Player.Empt)
                        if (InsertMove(row, col, i, col) == false)
                            return false;
                        else
                            finish = true;
                }

                if (board.GetChessPlayer(i, col) != Board.Player.Empt)
                    barrier = true;
            }

            // search to bottom
            finish = false;
            barrier = false;
            for (i = (row + 1); i < Board.BoardHeight && !finish; i++)
            {
                if (!barrier && board.GetChessPlayer(i, col) == Board.Player.Empt &&
                   InsertMove(row, col, i, col) == false)
                    return false;

                if (barrier)
                {
                    // barrier and see my side. finish
                    if (board.GetChessPlayer(i, col) == myside)
                        finish = true;

                    // barrier and see enemy. finish
                    else if (board.GetChessPlayer(i, col) != Board.Player.Empt)
                        if (InsertMove(row, col, i, col) == false)
                            return false;
                        else
                            finish = true;
                }

                if (board.GetChessPlayer(i, col) != Board.Player.Empt)
                    barrier = true;
            }
            return true;
        }

        private bool EscortMove(Board board, int row, int col)
        {

            const int left = 3;
            const int rite = 6;
            Board.Player myside = board.GetChessPlayer(row, col);

            int top = 0, bot = 3;
            if (myside == Board.Player.Bot) { top = 7; bot = Board.BoardHeight; }

            int testrow, testcol;
            // up 1 left 1
            testrow = row - 1; testcol = col - 1;
            if (Inbound(testrow, testcol, left, rite, top, bot) &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;
            // bot 1 left 1
            testrow = row + 1; testcol = col - 1;
            if (Inbound(testrow, testcol, left, rite, top, bot) &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;
            // up 1 right 1
            testrow = row - 1; testcol = col + 1;
            if (Inbound(testrow, testcol, left, rite, top, bot) &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;
            // bot 1 right 1
            testrow = row + 1; testcol = col + 1;
            if (Inbound(testrow, testcol, left, rite, top, bot) &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;
            return true;
        }

        private bool JumboMove(Board board, int row, int col)
        {

            Board.Player myside = board.GetChessPlayer(row, col);

            int top = 0, bot = 5;
            if (myside == Board.Player.Bot) { top = 5; bot = Board.BoardHeight; }
            // up 2 left 2
            int testrow, testcol, barrierrow, barriercol;
            testrow = row - 2; testcol = col - 2;
            barrierrow = row - 1; barriercol = col - 1;
            if (Inbound(testrow, testcol, 0, Board.BoardWidth, top, bot) &&
                board.GetChessPlayer(barrierrow, barriercol) == Board.Player.Empt &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;
            // bot 2 left 2
            testrow = row + 2; testcol = col - 2;
            barrierrow = row + 1; barriercol = col - 1;
            if (Inbound(testrow, testcol, 0, Board.BoardWidth, top, bot) &&
                board.GetChessPlayer(barrierrow, barriercol) == Board.Player.Empt &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;
            // up 2 left  2
            testrow = row - 2; testcol = col + 2;
            barrierrow = row - 1; barriercol = col + 1;
            if (Inbound(testrow, testcol, 0, Board.BoardWidth, top, bot) &&
                board.GetChessPlayer(barrierrow, barriercol) == Board.Player.Empt &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;
            // bot 2 right 2
            testrow = row + 2; testcol = col + 2;
            barrierrow = row + 1; barriercol = col + 1;
            if (Inbound(testrow, testcol, 0, Board.BoardWidth, top, bot) &&
                board.GetChessPlayer(barrierrow, barriercol) == Board.Player.Empt &&
                myside != board.GetChessPlayer(testrow, testcol) &&
                InsertMove(row, col, testrow, testcol) == false)
                return false;

            return true;
        }

        private bool SoldierMove(Board board, int row, int col)
        {

            Board.Player myside = board.GetChessPlayer(row, col);
            int testrow, testcol;

            if (myside == Board.Player.Top)
            {
                // crossed river
                if (row > 4)
                {
                    // left
                    testrow = row; testcol = col - 1;
                    if (Inbound(testrow, testcol, 0, Board.BoardWidth, 0, Board.BoardHeight) &&
                        board.GetChessPlayer(testrow, testcol) != myside &&

                        InsertMove(row, col, testrow, testcol) == false)
                        return false;
                    // right
                    testrow = row; testcol = col + 1;
                    if (Inbound(testrow, testcol, 0, Board.BoardWidth, 0, Board.BoardHeight) &&
                        board.GetChessPlayer(testrow, testcol) != myside &&

                        InsertMove(row, col, testrow, testcol) == false)
                        return false;
                }
                // go down
                testrow = row + 1; testcol = col;
                if (Inbound(testrow, testcol, 0, Board.BoardWidth, 0, Board.BoardHeight) &&
                    board.GetChessPlayer(testrow, testcol) != myside &&
                    InsertMove(row, col, testrow, col) == false)
                    return false;
            }
            else if (myside == Board.Player.Bot)
            {
                // crossed river
                if (row < 5)
                {
                    // go left
                    testrow = row; testcol = col - 1;
                    if (Inbound(testrow, testcol, 0, Board.BoardWidth, 0, Board.BoardHeight) &&
                       board.GetChessPlayer(testrow, testcol) != myside &&

                       InsertMove(row, col, testrow, testcol) == false)
                        return false;
                    // right
                    testrow = row; testcol = col + 1;
                    if (Inbound(testrow, testcol, 0, Board.BoardWidth, 0, Board.BoardHeight) &&
                        board.GetChessPlayer(testrow, testcol) != myside &&
                        InsertMove(row, col, testrow, testcol) == false)
                        return false;
                }
                // up
                testrow = row - 1; testcol = col;
                if (Inbound(testrow, testcol, 0, Board.BoardWidth, 0, Board.BoardHeight) &&
                   board.GetChessPlayer(testrow, testcol) != myside &&
                   InsertMove(row, col, testrow, col) == false)
                    return false;
            }
            return true;
        }
    }
}
