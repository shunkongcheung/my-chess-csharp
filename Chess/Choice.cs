﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    // helper structure for finding best route
    class Choice
    {
        // move
        public int fromrow, fromcol;
        public int torow, tocol;

        // result
        public Board.Player winner;
        public BoardVal boardValue;

        // for analysis
        public bool isAllCalculated;
                
        public Choice(Board board, Board.Player side)
        {
            fromrow = -1; fromcol = -1;
            torow = -1; tocol = -1;

            winner = Board.Player.Empt;
            boardValue = new BoardVal(board, side);

            isAllCalculated = false;
        }

        public static void CopyChoice(Choice dst, Choice src)
        {
            dst.fromrow = src.fromrow;
            dst.fromcol = src.fromcol;

            dst.torow = src.torow;
            dst.tocol = src.tocol;

            dst.winner = src.winner;
            dst.boardValue.SetBotVal(src.boardValue.GetBotVal());
            dst.boardValue.SetTopVal(src.boardValue.GetTopVal());

        }

        public static void SetToWorseValue(Choice dst, Board.Player side)
        {
            dst.winner = (side == Board.Player.Top) ? Board.Player.Bot : Board.Player.Top;
            dst.boardValue.SetTopVal((side == Board.Player.Top) ? 0 : 500);
            dst.boardValue.SetBotVal((side == Board.Player.Bot) ? 0 : 500);
        }

        // helper function for comparing Choice
        // return true if c1 is better than c2
        public static int IsBetterChoice(Choice c1, Choice c2, Board.Player side)
        {
            // no difference in winner
            if (c1.winner == c2.winner)
            {
                // no difference in board value
                int c1Value = c1.boardValue.GetWeighted();
                int c2Value = c2.boardValue.GetWeighted();

                // equal. not better
                if (c1Value == c2Value)
                {
                    // the one with higher total value is better
                    // more reserved power
                    int c1Total = c1.boardValue.GetTopVal();
                    int c2Total = c2.boardValue.GetTopVal();

                    if (c1Total > c2Total) return 1;
                    else if (c1Total < c2Total) return -1;

                    // exactly same value
                    return 0;
                }


                // if it is a top player
                else if (side == Board.Player.Top)
                {
                    // value is better
                    if (c1Value > c2Value) return 1;
                    // value is worse
                    else if (c1Value < c2Value) return -1;
                }

                // if it is a bottom player
                else
                {
                    // value is better
                    if (c1Value < c2Value) return 1;
                    // value is worse
                    else if (c1Value > c2Value) return -1;
                }
            }
            // has difference in winner and c1 is winner
            else if (c1.winner == side) { return 1; }

            // c1 is empty, depends on c2 then
            else if (c1.winner == Board.Player.Empt)
            {
                if (c2.winner == side) return -1;
                else return 1;
            }
            // c1 is losing, and c2 is different, then c1 must be worse than c2
            return -1;
        }
    };
}
