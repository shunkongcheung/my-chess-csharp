﻿using System;

namespace Chess
{
    class AnalyzerTreeBuilder
    {
        // the next tree
        public static AnalyzerNode NextTree = null;
        
        
        // information (pre)
        public static int TreeDepth = 0;
        public static Board.Player StartSide;
        public static int SelfLimit = 0;
                
        // for buliding
        private static AnalyzerNode[] LevelNodes = null;


        public static void PreSetting(int depth, int limit, Board.Player iStartSide)
        {
            // setting 
            TreeDepth = depth;
            SelfLimit = limit;
            StartSide = iStartSide;

            LevelNodes = new AnalyzerNode[depth];
        }

        public static void InsertNode(
            int depth,
            int fromrow, int fromcol, int torow, int tocol,
            Board.Player iWinner,
            BoardVal iBoardVal, int iExpctedWeight,
            Board iBoard
            )
        {
            if(depth > TreeDepth || depth < 0)
            {
                Console.Error.WriteLine("BuildTree: ERROR! Invalid depth ({0}) to existing structure {1}.", depth, TreeDepth);
                return;
            }

            // create new node
            AnalyzerNode insert = new AnalyzerNode(
                fromrow, fromcol, torow, tocol,
                iWinner, iBoardVal, iExpctedWeight, iBoard);

            // insert to respective level
            if(depth != TreeDepth)
            {
                insert.SetNext(LevelNodes[depth]);
                LevelNodes[depth] = insert;
            }
            else
            {
                NextTree = insert;
            }

            // if there are something below my level. They are now my children
            int lowlevel = depth - 1;
            if(lowlevel >= 0)
            {
                if(LevelNodes[lowlevel] != null)
                {
                    insert.SetChildren(LevelNodes[lowlevel]);
                    LevelNodes[lowlevel] = null;
                }
            }
        }


    }

}
