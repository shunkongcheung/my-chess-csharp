﻿using System;

namespace Chess
{

    class Board
    {

        public const int BoardWidth = 9;
        public const int BoardHeight = 10;

        public enum Role
        {
            GENERAL = 'G',
            CHARIOT = 'C',
            HORSE = 'H',
            ARTILLERY = 'A',
            ESCORT = 'E',
            JUMBO = 'J',
            SOLDIER = 'S',
            NOTHING = 'K'
        }
        public enum Player
        {
            Top = 'T',
            Bot = 'B',
            Empt = 'p'
        }
        
        // helper structure
        private class Chess
        {
            // helper class
            public Role role;
            public Player side;
        };

        // value of this board
        private Chess[][] thisboard;
        
        public Board(string filename)
        {
            // initialize this board
            thisboard = new Chess[BoardHeight][];
            for (int i = 0; i < BoardHeight; i++)
            {
                thisboard[i] = new Chess[BoardWidth];
                for (int j = 0; j < BoardWidth; j++)
                    thisboard[i][j] = new Chess();
            }

            // read from file
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(filename);
            for (int linecount = 0; (line = file.ReadLine()) != null && linecount < BoardHeight; linecount++)
            {
                if (line.Length != BoardWidth)
                {   // error input file
                    Console.Error.Write("Board: reading from file {0}. Line {1} have invalid length {2}..", filename, linecount, line.Length);
                }
                else
                {
                    // normal value
                    for (int j = 0; j < BoardWidth; j++)
                    {
                        if (line[j] == '.')
                        {   // empty
                            thisboard[linecount][j].side = Player.Empt; thisboard[linecount][j].role = Role.NOTHING;
                        }
                        else if ('a' <= line[j] && line[j] <= 'z')
                        {   // bottom player
                            thisboard[linecount][j].side = Player.Bot; thisboard[linecount][j].role = (Role)(line[j] - 'a' + 'A');
                        }
                        else
                        {    // top player
                            thisboard[linecount][j].side = Player.Top; thisboard[linecount][j].role = (Role)line[j];
                        }
                    }
                }
            }
            file.Close();
        }

        public Board()
        {
            // initialize this board
            thisboard = new Chess[BoardHeight][];
            for (int i = 0; i < BoardHeight; i++)
            {
                thisboard[i] = new Chess[BoardWidth];
                for (int j = 0; j < BoardWidth; j++)
                    thisboard[i][j] = new Chess();
            }
        }

        public Board(Board copy)
        {

            // initialize this board
            thisboard = new Chess[BoardHeight][];
            for (int i = 0; i < BoardHeight; i++)
            {
                thisboard[i] = new Chess[BoardWidth];
                for (int j = 0; j < BoardWidth; j++)
                    thisboard[i][j] = new Chess();
            }
            
            // copy
            for (int i = 0; i < BoardHeight; i ++)
                for( int j = 0; j < BoardWidth; j++)
                {
                    thisboard[i][j].role = copy.thisboard[i][j].role;
                    thisboard[i][j].side = copy.thisboard[i][j].side;
                }
        }

        public Player Getwinner()
        {
            int i, j;
            bool topgeneral = false;
            bool botgeneral = false;

            for (i = 0; i < BoardHeight; i++)
            {
                for (j = 3; j <= 5; j++)
                {
                    if (thisboard[i][j].role == Role.GENERAL && thisboard[i][j].side == Player.Top) topgeneral = true;
                    if (thisboard[i][j].role == Role.GENERAL && thisboard[i][j].side == Player.Bot) botgeneral = true;
                }
            }

            if (!topgeneral) return Player.Bot;
            if (!botgeneral) return Player.Top;
            return Player.Empt;
        }

        public Role GetChessName(int i, int j) { return thisboard[i][j].role; }

        public Player GetChessPlayer(int i, int j) { return thisboard[i][j].side; }

        public void MoveBoard(int frow, int fcol, int trow, int tcol)
        {
            thisboard[trow][tcol].role = thisboard[frow][fcol].role;
            thisboard[trow][tcol].side = thisboard[frow][fcol].side;
            thisboard[frow][fcol].role = Role.NOTHING; thisboard[frow][fcol].side = Player.Empt;
        }

        
        public void SetBoard(int row, int col, Role role, Player side)
        {
            thisboard[row][col].role = role;
            thisboard[row][col].side = side;
        }

        public static bool IsSameBoard(Board b1, Board b2)
        {
            for (int i = 0; i < Board.BoardHeight; i++)
                for (int j = 0; j < Board.BoardWidth; j++)
                    if (b1.GetChessName(i, j) != b2.GetChessName(i, j) ||
                        b1.GetChessPlayer(i, j) != b2.GetChessPlayer(i, j))
                        return false;
            return true;
        }

    }
}
