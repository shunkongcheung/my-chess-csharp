﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Chess
{
    public partial class AnalyzerForm : Form
    {
        private class AnalyzeTreeList
        {
            // the content
            private AnalyzerNode root, curLevel;

            // current side
            private Board.Player curside;
            private int curDepth;
            private int limit;

            // other information
            private int calculatedNode;

            // A double linked list of Analyze Tree
            public AnalyzeTreeList prev, next;

            public AnalyzeTreeList(AnalyzerNode iRoot, Board.Player iStartSide, int iTotalDepth, int iLimit)

            {
                root = iRoot;
                curLevel = root;
                curside = iStartSide;
                curDepth = iTotalDepth;

                limit = iLimit;

                prev = null;
                next = null;
            }

            public void PostSettingUpdate(int cal)
            {
                calculatedNode = cal;
            }

            public AnalyzerNode GetCurNode() { return curLevel; }
            public Board.Player GetCurSide() { return curside; }
            public int GetCurDepth() { return curDepth; }
            public int GetLimit() { return limit; }
            public int GetCal() { return calculatedNode; }

            public void GoUp()
            {
                AnalyzerNode temp = curLevel.GetParent();
                if (temp != null)
                {
                    curLevel = temp;
                    curDepth++;
                    curside = (curside == Board.Player.Top) ? Board.Player.Bot : Board.Player.Top;
                }
            }
            public void GoDown()
            {
                AnalyzerNode temp = curLevel.GetChosenChild();
                if (temp != null)
                {
                    curLevel = temp;
                    curDepth--;
                    curside = (curside == Board.Player.Top) ? Board.Player.Bot : Board.Player.Top;
                }
            }
            public void GoIndex(int index)
            {
                AnalyzerNode temp = curLevel.GetIndexedChilden(index);
                if (temp != null)
                {
                    curLevel = temp;
                    curDepth--;
                    curside = (curside == Board.Player.Top) ? Board.Player.Bot : Board.Player.Top;
                }
            }
        }

        // sadly after some teting, realize if there is too many tree,
        // memory runs out. Therefore maximum number of Tree should be constricted.
        public const int TreeMaxSize = 8;

        // A list and a current List
        private AnalyzeTreeList curAnalyzeTree;


        // images
        int imageIndex;
        PictureBox[] pictures;

        public AnalyzerForm()
        {
            // form function
            InitializeComponent();
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            // do nothing on load
            curAnalyzeTree = null;
        }

        public void PushTreeList()
        {
            if (AnalyzerTreeBuilder.NextTree != null)
            {
                // find the new tree and add it to my list
                AnalyzeTreeList nextTree = new AnalyzeTreeList(AnalyzerTreeBuilder.NextTree, AnalyzerTreeBuilder.StartSide, AnalyzerTreeBuilder.TreeDepth, AnalyzerTreeBuilder.SelfLimit);

                if (curAnalyzeTree == null)
                    curAnalyzeTree = nextTree;
                else
                {
                    // push this into the list
                    AnalyzeTreeList findEnd = curAnalyzeTree;
                    while (findEnd.next != null)
                    {
                        findEnd = findEnd.next;
                    }
                    // insert
                    findEnd.next = nextTree;
                    nextTree.prev = findEnd;

                    // from end count to front
                    int countList = 0;
                    while(findEnd.prev != null)
                    {
                        countList++;
                        findEnd = findEnd.prev;
                    }

                    // if too large. Remove it from list
                    if(countList > TreeMaxSize)
                    {
                        findEnd.next.prev = null;
                        findEnd.next = null;
                    }
                }
                
                // update image
                UpdateWithCurrentTree();

                // ground pointer
                AnalyzerTreeBuilder.NextTree = null;
            }
        }

        private void B_up_Click(object sender, EventArgs e)
        {
            curAnalyzeTree.GoUp();
            UpdateWithCurrentTree();
        }
        private void B_dive_Click(object sender, EventArgs e)
        {
            curAnalyzeTree.GoDown();
            UpdateWithCurrentTree();
        }

        private void Lb_options_SelectedIndexChanged(object sender, EventArgs e)
        {
            curAnalyzeTree.GoIndex(lb_options.SelectedIndex);
            UpdateWithCurrentTree();
        }

        private void B_prev_Click(object sender, EventArgs e)
        {
            AnalyzeTreeList temp = curAnalyzeTree.prev;
            if (temp != null)
            {
                curAnalyzeTree = temp;
                UpdateWithCurrentTree();
            }
        }

        private void B_next_Click(object sender, EventArgs e)
        {
            AnalyzeTreeList temp = curAnalyzeTree.next;
            if (temp != null)
            {
                curAnalyzeTree = temp;
                UpdateWithCurrentTree();
            }
        }

        private void UpdateWithCurrentTree()
        {

            AnalyzerNode curNode = curAnalyzeTree.GetCurNode();

            // update this board info
            label1.Text = "level: " + curAnalyzeTree.GetCurDepth() + ". limit: " + curAnalyzeTree.GetLimit() +  "Cal: " + curAnalyzeTree.GetCal() ;
            
            if (curNode.fromrow != -1)
                label2.Text = "choice: (" + curNode.fromrow + ", " + curNode.fromcol + ": " + curNode.board.GetChessName(curNode.fromrow, curNode.fromcol) + ")->(" +
                                        curNode.torow + ", " + curNode.tocol + ": " + curNode.board.GetChessName(curNode.torow, curNode.tocol) + "). ";
            else
                label2.Text = "choice: (). ";
            label2.Text += "side: " + curAnalyzeTree.GetCurSide() + ". winner: " + curNode.winner+". ";

            label3.Text = "top: " + curNode.boardValue.GetTopVal() + ". bot: " + curNode.boardValue.GetBotVal();
            label3.Text += "expected: " + curNode.expectedweighted + ". actual: " + curNode.boardValue.GetWeighted() +"." ;

            // update next choice
            lb_options.Items.Clear();

            int childIndex = 0;
            for (childIndex = 0; ; childIndex++)
            {

                AnalyzerNode child = curNode.GetIndexedChilden(childIndex);
                if (child == null) break;

                // compare boards and find out what is the from row and to row
                int fromrow, fromcol, torow, tocol;
                fromrow = fromcol = torow = tocol = -1;
                for (int j = 0; j < Board.BoardHeight; j++)
                    for (int k = 0; k < Board.BoardWidth; k++)
                    {
                        // a different chess
                        if (curNode.board.GetChessName(j, k) != child.board.GetChessName(j, k))
                        {
                            if (child.board.GetChessPlayer(j, k) == Board.Player.Empt)
                            {
                                fromrow = j; fromcol = k;
                            }
                            else
                            {
                                torow = j; tocol = k;
                            }
                        }
                    }

                string item = "Move: " + childIndex + ": (" + fromrow + "," + fromcol + ")->(" + torow + "," + tocol + ")";
                item += ". Topscore: " + child.boardValue.GetTopVal();
                item += ". Botscore: " + child.boardValue.GetTopVal();
                item += ". Expected Weighted: " + child.expectedweighted + ". Actual Weighted: " + child.boardValue.GetWeighted();
                item += ". Winner: " + child.winner;
                lb_options.Items.Add(item);
            }


            // create images
            int left = pictureBox1.Location.X;
            int top = pictureBox1.Location.Y;
            int width = pictureBox1.Size.Width;
            int height = pictureBox1.Size.Height;

            left = left + (int)(width * 0.05);
            width = (int)(width * 0.9);

            int cheight = height / Board.BoardHeight;
            int cwidth = width / Board.BoardWidth;

            // remove old chess
            for (int i = 0; i < imageIndex; i++)
            {
                this.Controls.Remove(pictures[i]);
            }

            // count how many chess in next board
            imageIndex = 0;
            for (int i = 0; i < Board.BoardHeight; i++)
                for (int j = 0; j < Board.BoardWidth; j++)
                {
                    if (curNode.board.GetChessPlayer(i, j) != Board.Player.Empt)
                        imageIndex++;
                }
            pictures = new PictureBox[imageIndex];
            for (int i = 0; i < imageIndex; i++)
            {
                pictures[i] = new PictureBox();
                pictures[i].Size = new Size(cwidth, cheight);
                pictures[i].SizeMode = PictureBoxSizeMode.StretchImage;
                pictures[i].BackColor = Color.Transparent;
                Controls.Add(pictures[i]);
                pictures[i].BringToFront();

            }

            // create pictures
            imageIndex = 0;
            for (int i = 0; i < Board.BoardHeight; i++)
                for (int j = 0; j < Board.BoardWidth; j++)
                {
                    Board.Role cname = curNode.board.GetChessName(i, j);
                    Board.Player cplayer = curNode.board.GetChessPlayer(i, j);

                    switch (cname)
                    {
                        case Board.Role.GENERAL:
                            if (cplayer == Board.Player.Top) pictures[imageIndex].Image = Properties.Resources.redgeneral;
                            else pictures[imageIndex].Image = Properties.Resources.blackgeneral;
                            pictures[imageIndex++].Location = new Point(left + cwidth * j, top + cheight * i);
                            break;

                        case Board.Role.CHARIOT:
                            if (cplayer == Board.Player.Top) pictures[imageIndex].Image = Properties.Resources.redchariot;
                            else pictures[imageIndex].Image = Properties.Resources.blackchariot;
                            pictures[imageIndex++].Location = new Point(left + cwidth * j, top + cheight * i);
                            break;

                        case Board.Role.HORSE:
                            if (cplayer == Board.Player.Top) pictures[imageIndex].Image = Properties.Resources.redhorse;
                            else pictures[imageIndex].Image = Properties.Resources.blackhorse;
                            pictures[imageIndex++].Location = new Point(left + cwidth * j, top + cheight * i);
                            break;

                        case Board.Role.ARTILLERY:
                            if (cplayer == Board.Player.Top) pictures[imageIndex].Image = Properties.Resources.redartillary;
                            else pictures[imageIndex].Image = Properties.Resources.blackartillary;
                            pictures[imageIndex++].Location = new Point(left + cwidth * j, top + cheight * i);
                            break;

                        case Board.Role.ESCORT:
                            if (cplayer == Board.Player.Top) pictures[imageIndex].Image = Properties.Resources.redescort;
                            else pictures[imageIndex].Image = Properties.Resources.blackescort;
                            pictures[imageIndex++].Location = new Point(left + cwidth * j, top + cheight * i);
                            break;

                        case Board.Role.JUMBO:
                            if (cplayer == Board.Player.Top) pictures[imageIndex].Image = Properties.Resources.redjumbo;
                            else pictures[imageIndex].Image = Properties.Resources.blackjumbo;
                            pictures[imageIndex++].Location = new Point(left + cwidth * j, top + cheight * i);
                            break;
                        case Board.Role.SOLDIER:
                            if (cplayer == Board.Player.Top) pictures[imageIndex].Image = Properties.Resources.redsoldier;
                            else pictures[imageIndex].Image = Properties.Resources.blacksoldier;
                            pictures[imageIndex++].Location = new Point(left + cwidth * j, top + cheight * i);
                            break;

                        default: break;
                    }
                }
        }


    }
}
