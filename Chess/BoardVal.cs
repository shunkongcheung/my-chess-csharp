﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    class BoardVal
    {
        private int topval, botval;

        public BoardVal(Board board, Board.Player side)
        {

            topval = 0;
            botval = 0;

            // top and bot val
            int i, j;
            for (i = 0; i < Board.BoardHeight; i++)
                for (j = 0; j < Board.BoardWidth; j++)
                {
                    if (board.GetChessPlayer(i, j) == Board.Player.Top)
                    {
                        if (board.GetChessName(i, j) == Board.Role.GENERAL) topval += 70;
                        if (board.GetChessName(i, j) == Board.Role.CHARIOT) topval += 5;
                        if (board.GetChessName(i, j) == Board.Role.HORSE) topval += 4;
                        if (board.GetChessName(i, j) == Board.Role.ARTILLERY) topval += 4;
                        if (board.GetChessName(i, j) == Board.Role.ESCORT) topval += 3;
                        if (board.GetChessName(i, j) == Board.Role.JUMBO) topval += 2;
                        if (board.GetChessName(i, j) == Board.Role.SOLDIER) topval += 1;
                    }
                    if (board.GetChessPlayer(i, j) == Board.Player.Bot)
                    {
                        if (board.GetChessName(i, j) == Board.Role.GENERAL) botval += 70;
                        if (board.GetChessName(i, j) == Board.Role.CHARIOT) botval += 5;
                        if (board.GetChessName(i, j) == Board.Role.HORSE) botval += 4;
                        if (board.GetChessName(i, j) == Board.Role.ARTILLERY) botval += 4;
                        if (board.GetChessName(i, j) == Board.Role.ESCORT) botval += 3;
                        if (board.GetChessName(i, j) == Board.Role.JUMBO) botval += 2;
                        if (board.GetChessName(i, j) == Board.Role.SOLDIER) botval += 1;
                    }
                }
        }

        public int GetTopVal() { return topval; }
        public int GetBotVal() { return botval; }
        public int GetWeighted() { return topval - botval; }
        public int GetTotalValue() { return topval + botval; }

        public void SetTopVal(int val)  { topval = val; }
        public void SetBotVal(int val){botval = val;}

    };
}
