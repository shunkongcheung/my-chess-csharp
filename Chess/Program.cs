﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Chess
{
    static class Program
    {
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // start visual
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // testing and setting
            Test.Testing();

            // switching between forms
            bool running = true;
            while(running)
            {
                switch (GameContent.CurGameState)
                {
                    case GameContent.GameState.Start: Application.Run(new OptionsForm()); break;
                    case GameContent.GameState.Pause: Application.Run(new OptionsForm());break;
                    case GameContent.GameState.Playing: Application.Run(new GameForm()); break;
                    case GameContent.GameState.Finish: running = false;  break;
                }
            }
        }
    }
}
