﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Chess
{

    public partial class GameForm : Form
    {
        // grpahics
        const int TitleHeight = 25;
        int WindowLeft, WindowWidth, WindowTop, WindowHeight, ChessHeight, ChessWidth;

        // Chess Images
        private int ChessCount;
        private PictureBox[] ChessImage;

        // current drawing state
        private enum GraphicState
        {
            steady,
            jumping,
            walking
        };

        private Mutex ReadWriteState = new Mutex();
        private GraphicState CurGraphicState;

        // selected Chess
        private PictureBox SelectedImage;

        // jumping animation
        private int JumpMax, JumpOrigin;

        // path pmoving Animagtion
        private Point PathToPixel;
        
        public GameForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            // graphics parameters
            int formWidth = Size.Width;
            int formHeight = Size.Height;

            WindowLeft = (int)(formWidth * 0.025); WindowWidth = (int)(formWidth * 0.95);
            WindowTop = (int)(formHeight * 0.001); WindowHeight = (int)(formHeight * 0.95);

            ChessHeight = WindowHeight / (Board.BoardHeight); ChessWidth = WindowWidth / (Board.BoardWidth);

            // reset chess Image
            ChessCount = 0; ChessImage = null;

            // graphics state
            ReadWriteState.WaitOne();
            CurGraphicState = GraphicState.steady;
            ReadWriteState.ReleaseMutex();

            // reset selected
            SelectedImage = null;

            // anamiation
            PathToPixel = new Point(-1, -1);
            
            // update once
            UpdateChessImages();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GameContent.CurGameState = GameContent.GameState.Pause;
            Close();
        }

        private void GameForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(GameContent.CurGameState == GameContent.GameState.Playing)
                GameContent.CurGameState = GameContent.GameState.Finish;
        }
        
        private Point MouseToBoardPosition(Point pixel)
        {
            Point position = new Point(
                (pixel.Y - Location.Y - TitleHeight - WindowTop) / ChessHeight,
                (pixel.X - Location.X - WindowLeft) / ChessWidth
                );

            if (position.X < 0 || position.X >= Board.BoardHeight ||
                position.Y < 0 || position.Y >= Board.BoardWidth)
                return new Point(-1, -1);

            return position;
        }

        private Point PixelToBoardPosition(Point pixel)
        {
            return new Point(
                (pixel.Y - WindowTop) / ChessHeight,
                (pixel.X - WindowLeft) / ChessWidth
                );
        }

        private void GameForm_KeyDown(object sender, KeyEventArgs e)
        {
            Keys key = e.KeyCode;
            if (GameContent.EnterSecretKey((char)key))
                UpdateChessImages();
            
        }

        private Point BoardPositionToPixel(Point position)
        {
            return new Point(
                position.Y * ChessWidth + WindowLeft,
                position.X * ChessHeight + WindowTop
                );
        }

        private void BackgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            // wait for first animation to be done
            bool escape = false;
            while (!escape)
            {
                ReadWriteState.WaitOne();
                escape = (CurGraphicState == GraphicState.steady);
                ReadWriteState.ReleaseMutex();
            }


            // lost
            if (GameContent.GetGameBoard().Getwinner() != Board.Player.Empt)
                return;

            // computer move
            var watch = System.Diagnostics.Stopwatch.StartNew();
            Choice choice = Choose.MakeChoice(GameContent.GetGameBoard(), Board.Player.Top);
            long temptime = watch.ElapsedMilliseconds;

            StringWriter str = new StringWriter();
            str.Write("Comp: Used {0} millisecond.", temptime);
            Test.OutputString(str.ToString());

            // update analyze form
            if (Test.IsRunAnalyzer())
                GameContent.Analyzer.PushTreeList();

            // if it is a valid move
            if (choice.fromrow != -1)
            {
                Test.OutputChoice(choice, GameContent.GetGameBoard());
                
                // move 
                GameContent.MoveGameBoard(Board.Player.Top, choice.fromrow, choice.fromcol, choice.torow, choice.tocol);

                // set path
                ReadWriteState.WaitOne();
                PathToPixel = BoardPositionToPixel(new Point(choice.torow, choice.tocol));
                Point fromPixel = BoardPositionToPixel(new Point(choice.fromrow, choice.fromcol));
                for (int i = 0; i < ChessCount; i++)
                {
                    // find the respective chesss
                    if (ChessImage[i].Location.X == fromPixel.X && ChessImage[i].Location.Y == fromPixel.Y)
                    {
                        SelectedImage = ChessImage[i];
                        CurGraphicState = GraphicState.walking;
                    }
                }
                ReadWriteState.ReleaseMutex();
            }

        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            const int numCycle = 10;

            ReadWriteState.WaitOne();
            switch (CurGraphicState)
            {
                // update graphics 
                case GraphicState.steady:
                    break;

                // continue to jump. Until state change
                case GraphicState.jumping:

                    if (SelectedImage.Location.Y < JumpMax)
                        SelectedImage.Location = new Point(SelectedImage.Location.X, JumpOrigin);
                    else
                        SelectedImage.Location = new Point(SelectedImage.Location.X, SelectedImage.Location.Y - 2);
                    break;

                // walk until point is reached
                case GraphicState.walking:

                    // calculate forwarding
                    int diffx = (PathToPixel.X - SelectedImage.Location.X) / numCycle;
                    int diffy = (PathToPixel.Y - SelectedImage.Location.Y) / numCycle;

                    // if distance too short. Consider it done
                    if (Math.Abs(diffx) < 5 && Math.Abs(diffy) < 5)
                    {
                        SelectedImage = null;
                        PathToPixel = new Point(-1, -1);

                        UpdateChessImages();
                        CurGraphicState = GraphicState.steady;
                    }
                    // continue to move
                    else
                    {
                        SelectedImage.Location = new Point(SelectedImage.Location.X + diffx, SelectedImage.Location.Y + diffy);
                    }

                    break;

                default: break;
            }
            ReadWriteState.ReleaseMutex();
        }

        private void ChessOnClick(object sender, EventArgs e)
        {
            // If winner is obtained
            if (GameContent.GetGameBoard().Getwinner() != Board.Player.Empt)
            {
               // GameContent.CurGameState = GameContent.GameState.Start;
                Close();
            }

            // if is waiting. Escape
            ReadWriteState.WaitOne();
            bool isWalking = (CurGraphicState == GraphicState.walking);
            ReadWriteState.ReleaseMutex();

            // if is in motion. also ignore
            if (isWalking)
                return;

            // calculate position 
            Point position = MouseToBoardPosition(MousePosition);

            // error input. Ignore
            if (position.X == -1) return;

            // did not select any image
            if (SelectedImage == null)
            {
                // only picture box works here
                if (sender.GetType() != typeof(PictureBox)) return;
                
                SelectedImage = (PictureBox)sender;
                JumpMax = SelectedImage.Location.Y - 15;
                JumpOrigin = SelectedImage.Location.Y;

                ReadWriteState.WaitOne();
                CurGraphicState = GraphicState.jumping;
                ReadWriteState.ReleaseMutex();

                Point fromPosition = PixelToBoardPosition(SelectedImage.Location);
                Console.WriteLine("click: ({0}, {1}) ", fromPosition.X, fromPosition.Y);
            }

            // selected image
            else
            {
                // check if it is a valid move
                Point fromPosition = PixelToBoardPosition(new Point(SelectedImage.Location.X, JumpOrigin) );

                // mark down current position
                PathToPixel = BoardPositionToPixel(position);
                if (GameContent.MoveGameBoard( Board.Player.Bot, fromPosition.X, fromPosition.Y, position.X, position.Y))
                {
                    
                    // start graphic 
                    ReadWriteState.WaitOne();
                    CurGraphicState = GraphicState.walking;
                    ReadWriteState.ReleaseMutex();

                    // start computer thread
                    backgroundWorker1.RunWorkerAsync();
                }
                else
                {
                    ReadWriteState.WaitOne();
                    UpdateChessImages();
                    CurGraphicState = GraphicState.steady;
                    SelectedImage = null;
                    ReadWriteState.ReleaseMutex();
                }
            }

        }

        private void UpdateChessImages()
        {
            // Update Image according to board
           
            // remove existing chess image
            for (int cIndex = 0; cIndex < ChessCount; cIndex++)
                this.Controls.Remove(ChessImage[cIndex]);

            // count existing chess
            ChessCount = 0;
            for (int i = 0; i < Board.BoardHeight; i++)
            {
                for (int j = 0; j < Board.BoardWidth; j++)
                {
                    if (GameContent.GetGameBoard().GetChessPlayer(i, j) != Board.Player.Empt)
                        ChessCount++;
                }
            }

            // initialize image
            ChessImage = new PictureBox[ChessCount];
            for (int i = 0; i < ChessCount; i++)
            {
                ChessImage[i] = new PictureBox
                {
                    Size = new Size(ChessWidth, ChessHeight),
                    SizeMode = PictureBoxSizeMode.StretchImage,
                    BackColor = Color.Transparent
                };
                Controls.Add(ChessImage[i]);
                ChessImage[i].BringToFront();
                ChessImage[i].MouseClick += new MouseEventHandler(ChessOnClick);

            }
            // initialize chess Image
            int imageIndex = 0;
            for (int i = 0; i < Board.BoardHeight; i++)
            {
                for (int j = 0; j < Board.BoardWidth; j++)
                {
                    Board.Role cname = GameContent.GetGameBoard().GetChessName(i, j);
                    Board.Player cplayer = GameContent.GetGameBoard().GetChessPlayer(i, j);

                    switch (cname)
                    {
                        case Board.Role.GENERAL:
                            if (cplayer == Board.Player.Top) ChessImage[imageIndex].Image = Properties.Resources.redgeneral;
                            else ChessImage[imageIndex].Image = Properties.Resources.blackgeneral;
                            ChessImage[imageIndex++].Location = BoardPositionToPixel(new Point(i, j));
                            break;

                        case Board.Role.CHARIOT:
                            if (cplayer == Board.Player.Top) ChessImage[imageIndex].Image = Properties.Resources.redchariot;
                            else ChessImage[imageIndex].Image = Properties.Resources.blackchariot;
                            ChessImage[imageIndex++].Location = BoardPositionToPixel(new Point(i, j));
                            break;

                        case Board.Role.HORSE:
                            if (cplayer == Board.Player.Top) ChessImage[imageIndex].Image = Properties.Resources.redhorse;
                            else ChessImage[imageIndex].Image = Properties.Resources.blackhorse;
                            ChessImage[imageIndex++].Location = BoardPositionToPixel(new Point(i, j));
                            break;

                        case Board.Role.ARTILLERY:
                            if (cplayer == Board.Player.Top) ChessImage[imageIndex].Image = Properties.Resources.redartillary;
                            else ChessImage[imageIndex].Image = Properties.Resources.blackartillary;
                            ChessImage[imageIndex++].Location = BoardPositionToPixel(new Point(i, j));
                            break;

                        case Board.Role.ESCORT:
                            if (cplayer == Board.Player.Top) ChessImage[imageIndex].Image = Properties.Resources.redescort;
                            else ChessImage[imageIndex].Image = Properties.Resources.blackescort;
                            ChessImage[imageIndex++].Location = BoardPositionToPixel(new Point(i, j));
                            break;

                        case Board.Role.JUMBO:
                            if (cplayer == Board.Player.Top) ChessImage[imageIndex].Image = Properties.Resources.redjumbo;
                            else ChessImage[imageIndex].Image = Properties.Resources.blackjumbo;
                            ChessImage[imageIndex++].Location = BoardPositionToPixel(new Point(i, j));
                            break;
                        case Board.Role.SOLDIER:
                            if (cplayer == Board.Player.Top) ChessImage[imageIndex].Image = Properties.Resources.redsoldier;
                            else ChessImage[imageIndex].Image = Properties.Resources.blacksoldier;
                            ChessImage[imageIndex++].Location = BoardPositionToPixel(new Point(i, j));
                            break;

                        default: break;
                    }
                }
            }

            button1.BringToFront();
        }

        private void PrintBoard(Board board)
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 9; j++)
                {
                    char chess = (char)board.GetChessName(i, j);
                    chess = chess == 'K' ? '.' : chess;
                    Console.Write(chess);
                }
                Console.WriteLine();
            }

        }

    }
}
