﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    class AnalyzerNode
    {
        // information about the choice
        public int fromrow, fromcol, torow, tocol;

        public Board.Player winner;
        public BoardVal boardValue;
        public int expectedweighted;

        // this board
        public Board board;

        // tree structure
        private AnalyzerNode parent;
        private AnalyzerNode[] children;
        
        private int childCount;
        private int chosenChild;

        // list structure
        private AnalyzerNode next;
        
        public AnalyzerNode(int ifromRow, int ifromCol, int iToRow, int iToCol,
                       Board.Player iWinner,
                       BoardVal iBoardVal, int iExpctedWeight,
                       Board iBoard)
        {
            // information about the choice
            fromrow = ifromRow; fromcol = ifromCol;
            torow = iToRow; tocol = iToCol;

            winner = iWinner;
            boardValue = iBoardVal;
            expectedweighted = iExpctedWeight;

            // the board. Dynamic allocate 2D array
            board = new Board(iBoard);

            // the choice. Initialization of child pointer will be done later
            parent = null;
            children = null;
            childCount = 0;
            chosenChild = -1;

            next = null;
        }

        public AnalyzerNode GetIndexedChilden(int index)
        {
            if (index >= 0 && index < childCount)   return children[index];
            return null;
        }

        public AnalyzerNode GetChosenChild()
        {
            if (chosenChild == -1) return null;
            return children[chosenChild];
        }

        public AnalyzerNode GetParent()  { return parent;}

        public void SetNext(AnalyzerNode iNext){ next = iNext; }

        public void SetChildren(AnalyzerNode list)
        {
            // given a list of node. Put them all into children
            // list is using the same AnalyzerNode structure.
            // use first child pointer to next

            // count how many children in this list
            AnalyzerNode loop = list;
            while (loop != null)
            {
                childCount++;
                loop = loop.next;
            }

            // allocate children
            children = new AnalyzerNode[childCount];

            // find the chosen childen
            Board expectBoard = new Board(board);
            if (fromrow != -1) { expectBoard.MoveBoard(fromrow, fromcol, torow, tocol); }


            // put them all into list
            loop = list;
            for (int i = 0; i < childCount; i++)
            {
                if(loop == null)
                {
                    Console.Error.WriteLine("AnalyzerBuild: Loop is null!");
                        return;
                }
                // build tree strucutre
                children[i] = loop;
                children[i].parent = this;
                
                // move loop
                loop = loop.next;
                children[i].next = null;

                // chosen child found
                if (Board.IsSameBoard(children[i].board, expectBoard))
                    chosenChild = i;
            }
            
        }
    }
}
