﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Chess
{
    public partial class OptionsForm : Form
    {

        public OptionsForm()
        {
            InitializeComponent();
        }

        private void OptionsForm_Load(object sender, EventArgs e)
        {
            // start game. Or Resume Game
            if (GameContent.CurGameState == GameContent.GameState.Start)
            {
                GameContent.RestartGameBoard();
                button7.Text = "Start";
            }
            else
                button7.Text = "Resume";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // easy
            Choose.SetDifficulty(1);
            GameContent.CurGameState = GameContent.GameState.Playing;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // normal
            Choose.SetDifficulty(2);
            GameContent.CurGameState = GameContent.GameState.Playing;
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // difficult
            Choose.SetDifficulty(3);
            GameContent.CurGameState = GameContent.GameState.Playing;
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // regret twice, for both player
            GameContent.RegretMove();
            GameContent.RegretMove();
            GameContent.CurGameState = GameContent.GameState.Playing;
            Close();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            // change side
            GameContent.SwitchSide();
            GameContent.CurGameState = GameContent.GameState.Playing;
            Close();
        }
        
        private void button7_Click(object sender, EventArgs e)
        {
            // resume / restart
            GameContent.CurGameState = GameContent.GameState.Playing;
            Close();
        }
        
        private void button6_Click(object sender, EventArgs e)
        {
            // restart
            GameContent.RestartGameBoard();
            GameContent.CurGameState = GameContent.GameState.Playing;
            Close();
        }

        private void OptionsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(GameContent.CurGameState == GameContent.GameState.Start)
                GameContent.CurGameState = GameContent.GameState.Finish;
        }
    }
}
