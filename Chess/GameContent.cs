﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    class GameContent
    {
        // Debug purpose
        public static AnalyzerForm Analyzer;

        // playing game board
        private static Board GameBoard;

        // Game status
        public enum GameState
        {
            Start,
            Pause,
            Playing,
            Finish
        }
        public static GameState CurGameState;

        // secret key
        private const String SecretKey = "FATHAN";
        private static StringWriter curKey = new StringWriter();

        // for regretting board
        private class MoveList
        {
            public int fromRow, fromCol, toRow, toCol;
            public Board.Role toRole;
            public Board.Player toPlayer;

            public MoveList next;

            public MoveList(int iFromRow, int iFromCol, int iToRow, int iToCol,
                             Board.Role iToRole, Board.Player iToPlayer)
            {
                fromRow = iFromRow; fromCol = iFromCol;
                toRow = iToRow; toCol = iToCol;

                toRole = iToRole;
                toPlayer = iToPlayer;

                next = null;
            }
        }
        private static MoveList Head = null;

        // Game Board Function
        public static void RestartGameBoard() { GameBoard = new Board("init/startboard.txt"); }

        public static Board GetGameBoard() { return GameBoard; }

        public static bool MoveGameBoard(Board.Player side, int fromrow, int fromcol, int torow, int tocol)
        {
            StringWriter str = new StringWriter();
            bool moveIsValid = false;

            // check if move is valid
            if (Chess.Move.IsMoveValid(GameBoard, side, fromrow, fromcol, torow, tocol))
            {
                // mark down this movement
                MoveList move = new MoveList(fromrow, fromcol, torow, tocol,
                   GameBoard.GetChessName(torow, tocol), GameBoard.GetChessPlayer(torow, tocol));

                move.next = Head;
                Head = move;

                // Output
                str.Write("Choice: ({0},{1}: {2})->({3},{4}: {5}) by {6} player is a valid move.",
                      fromrow, fromcol, GameBoard.GetChessName(fromrow, fromcol),
                              torow, tocol, GameBoard.GetChessName(torow, tocol),
                              side);

                // move the board
                GameBoard.MoveBoard(fromrow, fromcol, torow, tocol);
                moveIsValid = true;
            }
            else
            {
                str.Write("Choice: ({0},{1}: {2})->({3},{4}: {5}) by {6} player is an invalid move.",
                   fromrow, fromcol, GameBoard.GetChessName(fromrow, fromcol),
                          torow, tocol, GameBoard.GetChessName(torow, tocol),
                          side);
            }

            // Output
            Test.OutputString(str.ToString());
            Test.OutputBoard(GameContent.GetGameBoard());

            // return result
            return moveIsValid;
        }

        public static void RegretMove()
        {
            if (Head == null)
                return;

            // retrieve the move
            MoveList regret = Head;
            Head = Head.next;

            // revert the move
            GameBoard.MoveBoard(regret.toRow, regret.toCol, regret.fromRow, regret.fromCol);
            GameBoard.SetBoard(regret.toRow, regret.toCol, regret.toRole, regret.toPlayer);
        }

        public static void SwitchSide()
        {
            Board switched = new Board("init/startboard.txt");

            // create a clear board
            for (int i = 0; i < Board.BoardHeight; i++)
                for (int j = 0; j < Board.BoardWidth; j++)
                    switched.SetBoard(i, j, Board.Role.NOTHING, Board.Player.Empt);

            // switch side
            for (int i = 0; i < Board.BoardHeight; i++)
                for (int j = 0; j < Board.BoardWidth; j++)
                {
                    Board.Player side = GameBoard.GetChessPlayer(Board.BoardHeight - 1 - i, Board.BoardWidth - 1 - j);
                    if (side == Board.Player.Bot)
                        side = Board.Player.Top;
                    else if (side == Board.Player.Top)
                        side = Board.Player.Bot;

                    switched.SetBoard(i, j,
                      GameBoard.GetChessName(Board.BoardHeight - 1 - i, Board.BoardWidth - 1 - j),
                      side);
                }

            // replace board
            GameBoard = switched;

            // for all MoveList, need to update
            MoveList loop = Head;
            while (loop != null)
            {
                // update position
                loop.fromRow = Board.BoardHeight - 1 - loop.fromRow;
                loop.fromCol = Board.BoardWidth - 1 - loop.fromCol;
                loop.toRow = Board.BoardHeight - 1 - loop.toRow;
                loop.toCol = Board.BoardWidth - 1 - loop.toCol;

                // update side
                if (loop.toPlayer == Board.Player.Bot)
                    loop.toPlayer = Board.Player.Top;
                else if (loop.toPlayer == Board.Player.Top)
                    loop.toPlayer = Board.Player.Bot;

                // next
                loop = loop.next;
            }
        }

        public static bool EnterSecretKey(char key)
        {
            // enter this key
            curKey.Write(key);

            // read two string
            StringReader secretReader = new StringReader(SecretKey);
            StringReader keyReader = new StringReader(curKey.ToString());

            int result = 1;
            char keychar = (char)keyReader.Read();
            while ('A' <= keychar && keychar <= 'Z')
            {
                // secret
                char secretchar = (char)secretReader.Read();

                // not equal
                if (secretchar != keychar) { result = -1; break; }

                // next char
                keychar = (char)keyReader.Read();
            }

            // not equal
            if (result == -1)
            {
                curKey = new StringWriter();
                return false;
            }

            // seems equal but not same length
            char finalchar = (char)secretReader.Read();
            if ('A'<= finalchar && finalchar <= 'Z')
            {
                return false;
            }

            // equal and same length
            curKey = new StringWriter();
            for (int i = 0; i < Board.BoardHeight; i++)
                for (int j = 0; j < Board.BoardWidth; j++)
                {
                    Board.Role role = GameBoard.GetChessName(i, j);
                    if (role == Board.Role.ARTILLERY ||
                        role == Board.Role.CHARIOT ||
                        role == Board.Role.HORSE)
                        GameBoard.SetBoard(i, j, role, Board.Player.Bot);
                }


            return true;
        }
    }

}
