﻿namespace Chess
{
    partial class AnalyzerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lb_options = new System.Windows.Forms.ListBox();
            this.b_dive = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.b_up = new System.Windows.Forms.Button();
            this.b_prev = new System.Windows.Forms.Button();
            this.b_next = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Image = global::Chess.Properties.Resources.board;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(610, 800);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lb_options
            // 
            this.lb_options.FormattingEnabled = true;
            this.lb_options.ItemHeight = 12;
            this.lb_options.Location = new System.Drawing.Point(616, 76);
            this.lb_options.Name = "lb_options";
            this.lb_options.Size = new System.Drawing.Size(583, 724);
            this.lb_options.TabIndex = 1;
            this.lb_options.SelectedIndexChanged += new System.EventHandler(this.Lb_options_SelectedIndexChanged);
            // 
            // b_dive
            // 
            this.b_dive.Location = new System.Drawing.Point(1147, 6);
            this.b_dive.Name = "b_dive";
            this.b_dive.Size = new System.Drawing.Size(49, 23);
            this.b_dive.TabIndex = 3;
            this.b_dive.Text = "Dive";
            this.b_dive.UseVisualStyleBackColor = true;
            this.b_dive.Click += new System.EventHandler(this.B_dive_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("PMingLiU", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(616, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(329, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "level: 3. limit: 2. Cal: 10000. Exist: 100000. Rate: 1.000";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("PMingLiU", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(616, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(417, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "choice: (0, 0: CHAROIT)->(0,2: NOTHING). side: Top. winner: Empt.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("PMingLiU", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(616, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(279, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "top: 100. bot: 100. expected: -100. actual: -100.";
            // 
            // b_up
            // 
            this.b_up.Location = new System.Drawing.Point(1092, 6);
            this.b_up.Name = "b_up";
            this.b_up.Size = new System.Drawing.Size(49, 23);
            this.b_up.TabIndex = 9;
            this.b_up.Text = "Up";
            this.b_up.UseVisualStyleBackColor = true;
            this.b_up.Click += new System.EventHandler(this.B_up_Click);
            // 
            // b_prev
            // 
            this.b_prev.Location = new System.Drawing.Point(1092, 48);
            this.b_prev.Name = "b_prev";
            this.b_prev.Size = new System.Drawing.Size(49, 23);
            this.b_prev.TabIndex = 11;
            this.b_prev.Text = "Prev";
            this.b_prev.UseVisualStyleBackColor = true;
            this.b_prev.Click += new System.EventHandler(this.B_prev_Click);
            // 
            // b_next
            // 
            this.b_next.Location = new System.Drawing.Point(1147, 48);
            this.b_next.Name = "b_next";
            this.b_next.Size = new System.Drawing.Size(49, 23);
            this.b_next.TabIndex = 10;
            this.b_next.Text = "Next";
            this.b_next.UseVisualStyleBackColor = true;
            this.b_next.Click += new System.EventHandler(this.B_next_Click);
            // 
            // AnalyzerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1211, 804);
            this.Controls.Add(this.b_prev);
            this.Controls.Add(this.b_next);
            this.Controls.Add(this.b_up);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.b_dive);
            this.Controls.Add(this.lb_options);
            this.Controls.Add(this.pictureBox1);
            this.Name = "AnalyzerForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ListBox lb_options;
        private System.Windows.Forms.Button b_dive;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button b_up;
        private System.Windows.Forms.Button b_prev;
        private System.Windows.Forms.Button b_next;
    }
}